package model.data_structures;

import java.util.Iterator;

public class IteradorListaSimple implements Iterator
{

	private NodoSencillo nS;
	
	public boolean hasNext() 
	{
		boolean resp = false;
		if(nS.tieneSiguiente() == true)
		{
			resp = true;
		}
		return resp;
	}

	public NodoSencillo next() 
	{
		NodoSencillo resp = null;
		if(nS.tieneSiguiente() == true)
		{
			resp = nS.darSiguiente();
		}
		return resp;
	}

	public void remove() 
	{	
	}
	
}
