package model.data_structures;

public class NodoDoble<T> {
	private T data;
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	private int posicionNodo;
	
	public NodoDoble(T pData, int pPosicion){
		data = pData;
		siguiente = null;
		anterior = null;
		posicionNodo = pPosicion;
		
	}

	public void asignarSiguiente(NodoDoble<T> pNodo)
	{
		siguiente = pNodo;
	}
	
	public void asignarAnterior(NodoDoble<T> pNodo)
	{
		anterior= pNodo;
	}
	
	public boolean tieneSiguiente()
	{
		boolean tiene = true;
		if(siguiente == null){
			tiene = false;
		}
		return tiene;
	}
	
	public NodoDoble<T> darSiguiente()
	{
		return siguiente;
	}
	
	public boolean tieneAnterior()
	{
		boolean tiene = true;
		if(anterior == null)
		{
			tiene = false;
		}
		return tiene;
	}
	public NodoDoble<T> darAnterior()
	{
		return anterior;
	}
	public T darData(){
		return data;
	}
	
	public int darPosicion(){
		return posicionNodo;
	}
}
