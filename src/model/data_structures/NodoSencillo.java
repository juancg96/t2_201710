package model.data_structures;

public class NodoSencillo<T> {

	private T data;
	private NodoSencillo<T> siguiente;
	private int posicionNodo;

	public NodoSencillo(T pData, int pPosicion)
	{
		data = pData;
		siguiente = null;
		posicionNodo = pPosicion;
	}

	public void asignarSiguiente(NodoSencillo<T> pNodo)
	{
		siguiente = pNodo;
	}
	public NodoSencillo<T> darSiguiente()
	{
		return siguiente;
	}
	public T darData()
	{
		return data;
	}
	public int darPosicion()
	{
		return posicionNodo;
	}
	public boolean tieneSiguiente()
	{
		boolean tiene = true;
		if(siguiente == null){
			tiene = false;
		}
		return tiene;
	}
}