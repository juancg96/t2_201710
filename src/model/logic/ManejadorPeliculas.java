package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoDoble;
import model.data_structures.NodoSencillo;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		//misPeliculas es una lista encadenada sencilla.
		//peliculasAgno es una lista doblemente encadenada.
		misPeliculas = new ListaEncadenada<VOPelicula>();
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();


		String csvFile = archivoPeliculas;
		BufferedReader br = null;
		String line = "";

		try {

			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();
			while ((line = br.readLine()) != null) {
				//Este codigo dentro del while se va a repetir para cada pelicula.


				//Se extraen los datos de nombre, año, y generos de la pelicula actual.
				String[] lineaActual = line.split(";");
				String titulo = lineaActual[1].split("(")[0];
				String anio = lineaActual[1].split("(")[1].split(")")[0];
				int agnoPublicacion = Integer.parseInt(anio);


				//Se extraen los nombres de los generos.
				String generos[] = lineaActual[2].split("|");


				//Se agregan los generos a una ILista.
				ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>(); 
				for(int i = 0; i<generos.length; i++)
				{
					listaGeneros.agregarElementoFinal(generos[i]);
				}

				//Se crea una VOPelicula y se asignan los valores de la pelicula actual.
				VOPelicula actual = new VOPelicula();
				actual.setTitulo(titulo);
				actual.setAgnoPublicacion(agnoPublicacion);
				actual.setGenerosAsociados(listaGeneros);

				//Se agrega el objecto VOPelicula a la lista misPeliculas. 
				misPeliculas.agregarElementoFinal(actual);

			}
			
			//Ahora se recorre la lista y se crea peliculasAgno
			for(int i = 1950; i<=2016; i++){
				int siHay = 0;
				ILista<VOPelicula> listaPeliculasDeEsteAnio = new ListaEncadenada<VOPelicula>();

				for(int j = 0; j<misPeliculas.darNumeroElementos(); j++){
					if(misPeliculas.darElemento(j).getAgnoPublicacion() == i){
						siHay++;
						listaPeliculasDeEsteAnio.agregarElementoFinal(misPeliculas.darElemento(j));
					}
				}
				//Si hay peliculas de el año que se acabo de recorrer, se agregan a peliculasAgno
				if(siHay != 0){
					VOAgnoPelicula agnoActual = new VOAgnoPelicula();
					agnoActual.setAgno(i);
					agnoActual.setPeliculas(listaPeliculasDeEsteAnio);
					peliculasAgno.agregarElementoFinal(agnoActual);	
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public ILista<VOPelicula> darListaPeliculas(String busqueda)
	{
		ILista<VOPelicula> listaResp = new ListaEncadenada<VOPelicula>();
		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++)
		{
			if(misPeliculas.darElemento(i).getTitulo().toLowerCase().indexOf(busqueda.toLowerCase()) != -1)
			{
				listaResp.agregarElementoFinal(misPeliculas.darElemento(i));
			}
		}
		return listaResp;
	}
	
	public ILista<VOPelicula> darPeliculasAgno(int agno) 
	{
		ILista<VOPelicula> r = null;
		for(int i = 0; i<peliculasAgno.darNumeroElementos(); i++){
			if(peliculasAgno.darElemento(i).getAgno() == agno){
				r = peliculasAgno.darElemento(i).getPeliculas();
			}
		}
		return r;
	}

	
	public VOAgnoPelicula darPeliculasAgnoSiguiente() 
	{
		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();
	}

	public VOAgnoPelicula darPeliculasAgnoAnterior() 
	{
		peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
		
	}

}
