package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> primerNodo;
	private NodoDoble<T> ultimoNodo;
	private NodoDoble<T> nodoReferencia;
	private int cantidadNodos;


	public ListaDobleEncadenada()
	{
		primerNodo = null;
		ultimoNodo = null;
		nodoReferencia = null;
		cantidadNodos = 0;

	}

	@Override
	public Iterator<T> iterator() 
	{

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) 
	{


		if(cantidadNodos==0){
			NodoDoble<T> p= new NodoDoble<T>(elem, 0);
			primerNodo = p;
			ultimoNodo = p;
			nodoReferencia = p;
		}else{
			NodoDoble<T> p= new NodoDoble<T>(elem, ultimoNodo.darPosicion() +1);
			ultimoNodo.asignarSiguiente(p);

			p.asignarAnterior(ultimoNodo);

			ultimoNodo = p;
		}
		cantidadNodos++;

	}


	@Override
	public T darElemento(int pos) 
	{

		NodoDoble<T> nodo = null;
		NodoDoble<T> nodoActual = primerNodo;

		//Revisa si se quiere el primero.
		if(pos == 0)
		{
			nodo = primerNodo;
		}

		while(nodoActual.darSiguiente() != null){
			if(nodoActual.darPosicion() == pos){
				nodo = nodoActual;
			}
			nodoActual.darSiguiente();
		}
		nodoReferencia = nodo;
		return nodo.darData();
	}
	@Override
	public int darNumeroElementos() {
		return cantidadNodos;
	}

	@Override
	public T darElementoPosicionActual() {
		return nodoReferencia.darData();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if(nodoReferencia.darSiguiente() == null){
			return false;
		}
		else{
			nodoReferencia = nodoReferencia.darSiguiente();
			return true;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if(nodoReferencia.darAnterior() == null){
			return false;
		}
		else{
			nodoReferencia = nodoReferencia.darAnterior();
			return true;
		}
	}

}