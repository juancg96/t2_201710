package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primerNodo;
	private NodoSencillo<T> nodoReferencia;
	private int cantidadNodos;


	public ListaEncadenada()
	{
		primerNodo = null;
		nodoReferencia = null;
		cantidadNodos = 0;

	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {

		NodoSencillo<T> nodoActual = primerNodo;

		//Revisa si no hay nodos.
		if(primerNodo == null){
			primerNodo = new NodoSencillo<T>(elem, 0);
			nodoReferencia = primerNodo;
		}
		else{
			//Si hay nodos, recorre la lista hasta el final
			int posicionesRecorridas = 0;
			while(nodoActual.darSiguiente() != null){
				nodoActual = nodoActual.darSiguiente();
				posicionesRecorridas++;
				nodoActual.darSiguiente();
			}
			
			nodoActual.asignarSiguiente(new NodoSencillo<T>(elem, posicionesRecorridas));
		}
		cantidadNodos++;
	}

	@Override
	public T darElemento(int pos) 
	{

		NodoSencillo<T> nodo = null;
		NodoSencillo<T> nodoActual = primerNodo;

		//Revisa si se quiere el primero.
		if(pos == 0)
		{
			nodo = primerNodo;
		}

		while(nodoActual.darSiguiente() != null){
			if(nodoActual.darPosicion() == pos){
				nodo = nodoActual;
			}
			nodoActual.darSiguiente();
		}
		nodoReferencia = nodo;
		return nodo.darData();
	}


	@Override
	public int darNumeroElementos() {

		return cantidadNodos;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		
		return nodoReferencia.darData();
	}

	@Override
	public boolean avanzarSiguientePosicion() {

		if(nodoReferencia.tieneSiguiente()){
			nodoReferencia = nodoReferencia.darSiguiente();
			return true;
		}
		else{
			return false;
		}
	}
	
	public NodoSencillo<T> darElementoSiguiente()
	{
	
		return primerNodo.darSiguiente();
		
	}
	@Override
	public boolean retrocederPosicionAnterior() 
	{

		return false;
	}

}
