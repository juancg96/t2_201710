package model.data_structures;

import java.util.Iterator;

public class IteradorListaDoble implements Iterator

{
	private NodoDoble nD;

	@Override
	public boolean hasNext()
	{
		return nD.tieneSiguiente();
	}

	@Override
	public NodoDoble next()
	{
		return nD.darSiguiente();
	}
	
	public boolean hasPrevious()
	{
		return nD.tieneAnterior();
	}
	
	public NodoDoble previous()
	{
		return nD.darAnterior();
	}

	@Override
	public void remove() 
	{

	}

	
	
}
